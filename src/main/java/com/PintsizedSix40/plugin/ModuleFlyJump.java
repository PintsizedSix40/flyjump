package com.PintsizedSix40.plugin;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

public class ModuleFlyJump extends Module {

  public ModuleFlyJump() {
    super("FlyJump", ModuleCategory.MOVEMENT);

    this.setVersion("1.1");
    this.setBuildVersion(18201);
    this.setDescription("Set a keybind and spam it to fly/jump high. Only works on vanilla.");
    this.setRegistered(true);
  }

  @Override
  public void onEnable() {
    Wrapper.getPlayer().setPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + 5, Wrapper.getPlayer().posZ);
    Wrapper.getPlayer().motionY = 0;

    this.setEnabled(false);
  }

}